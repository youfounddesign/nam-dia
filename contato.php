<div class="ajax-page contact">
  <div class="page-content">

    <a href="javascript:void(0);" class="close-ajax">
      <i class="icon i25 icon-17"></i>
    </a>

    <div class="title-box">
      <h2>Contato</h2>
      <p>Fale com a gente! Atendimento diferenciado, ágil e alinhado com as necessidades dos nossos clientes e parceiros.</p>
    </div>

    <form method="post" action="">

      <div class="row">
        <input type="text" class="input-text" placeholder="Nome completo *" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Empresa" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Email *" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Telefone" phone />
      </div>

      <div class="row">
        <textarea class="input-text" placeholder="Como podemos lhe ajudar?" style="height: 137px !important;"></textarea>
      </div>

      <div class="button-bar">
        <input type="submit" value="Enviar" class="button large center" />
      </div>

    </form>

  </div>
</div>