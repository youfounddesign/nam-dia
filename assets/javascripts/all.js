window.defaultUrl = '';

// Browser Detect - Add Class to Body
$(document).ready(function(){var userAgent=navigator.userAgent.toLowerCase();$.browser.chrome=/chrome/.test(navigator.userAgent.toLowerCase());if($.browser.msie){$("body").addClass("ie");$("body").addClass("ie"+$.browser.version.substring(0,1))}if($.browser.chrome){$("body").addClass("chrome");userAgent=userAgent.substring(userAgent.indexOf("chrome/")+7);userAgent=userAgent.substring(0,1);$("body").addClass("chrome"+userAgent);$.browser.safari=false}if($.browser.safari){$("body").addClass("safari");
userAgent=userAgent.substring(userAgent.indexOf("version/")+8);userAgent=userAgent.substring(0,1);$("body").addClass("safari"+userAgent)}if($.browser.mozilla)if(navigator.userAgent.toLowerCase().indexOf("firefox")!=-1){$("body").addClass("firefox");userAgent=userAgent.substring(userAgent.indexOf("firefox/")+8);userAgent=userAgent.substring(0,1);$("body").addClass("firefox"+userAgent)}else $("body").addClass("firefox");if($.browser.opera)$("body").addClass("opera")});

$(document).ready(function() {

  /* Menu active */
  var url = location.href.substring(location.href.lastIndexOf("/")+1,255);
  $('nav a[href="'+url+'"]').addClass('active');

  if ($("*[data-banner]:first").length){
    $('*[data-banner]:first').bxSlider({
      mode: 'fade',
      pagerCustom: '.banner-controls',
      controls: false
    });
  }

  if ($("*[data-slider-photos]:first").length){
    $('*[data-slider-photos]:first').bxSlider({
      mode: 'fade',
      speed: 200,
      pagerCustom: '.slider-photos-controls',
      controls: false
    });
  }

  if ($("*[data-slider-testimonials]:first").length){
    $('*[data-slider-testimonials]:first').bxSlider({
      mode: 'horizontal',
      pager: false
    });
  }

  $('.collapse-menu').toggle(function() {
    $(".fixed-navigation").animate({ right: "0" });
  }, function(){
     $(".fixed-navigation").animate({ right: "-157px" }, "fast");
  });

  // Ajax pages
  $(".ajax-link").click(function(e){
    e.preventDefault();
    var href = $(this).attr('href');
    $(".hide-pages").load(href);

    $(document).ajaxComplete(function(){
      $('.ajax-page').find('.input-text:first').focus();

      // Após abrir paginas ajax
      $("body").animate({ left: "-600px"}, 'fast');
      $(".ajax-page").animate({ right: "0"}, 'fast');

      // Fechar páginas ajax
      $(".close-ajax").click(function(){
        $("body").animate({ left: "0"}, 'fast');
        $(".ajax-page").animate({ right: "-600px"}, 'fast');
      });

    });

  });

  /* Mask Form */
  $("input[date]").mask("99/99/9999");
  $("input[phone]").mask("(99) 9999-9999");
  $("input[rg]").mask("9.999.999.9");
  $("input[cpf]").mask("999.999.999-99");
  $("input[cep]").mask("99999-999");

  /* Fancy */
  $(".expand").fancybox({
    overlayShow : false,
    openEffect  : 'fade',
    closeEffect : 'fade',
    padding     : 15,
    margin      : 0
  });

});