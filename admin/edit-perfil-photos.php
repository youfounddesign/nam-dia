<div class="container edit-box">

  <div class="title-bar">
    <h2>Editar perfil</h2>
    <p>Bem vindo! Preencha o formulário e junte-se a esta grande <br>equipe de sucesso.</p>

    <div class="filter">
      <nav>
        <ul class="edit-menu">
          <li><a href="index.php?url=edit-perfil-geral" class="first">Geral</a></li>
          <li><a href="index.php?url=edit-perfil-professional">Profissional</a></li>
          <li><a href="index.php?url=edit-perfil-photos">Fotos</a></li>
          <li><a href="index.php?url=edit-perfil-videos">Vídeos</a></li>
          <li><a href="index.php?url=edit-perfil-curriculo">Currículo</a></li>
        </ul>
      </nav> 
    </div>

  </div>

  <div class="page-content">

    <form method="post" action="index.php?url=perfil-description">
      <div class="content-bar">

        <div class="send-upload">
          <h3 class="subtitle">Envio<br/> de fotos</h3>
          
          <div class="grid">
            <div class="grid-cell t50">

              <div class="instructions">
                <span>1</span>
                <h4> Instruções para o envio de fotos</h4>
                <p>
                  • As Fotos devem ser recentes tiradas há menos de 1 ano<br/>
                  • Não precisa ser foto de book profissional 
                </p>
              </div>

              <div class="instructions">
                <span>2</span>
                <h4>Fotos não aceitas</h4>
                <p>
                  • Fotos de baixa qualidade<br/>
                  • Fotos Pequenas<br/>
                  • Fotos com outras pessoas
                </p>
              </div>

            </div>
            <div class="grid-cell t50">
              <p class="obs">
                OBS: A NaMídia irá analisar as melhores fotos 
                enviadas e postará somente as que tiverem dentro do 
                padrão para o website NaMídia.

                O prazo de postagem é de até 7 dias após o cadastro.
              </p>
            </div>
          </div>

          <div class="grid col-top-medium">
            <div class="grid-cell t40">
              <ul class="list-upload">
                <li>
                  <span>Foto 1 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
                <li>
                  <span>Foto 2 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
                <li>
                  <span>Foto 3 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
                <li>
                  <span>Foto 4 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
                <li>
                  <span>Foto 5 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
                <li>
                  <span>Foto 6 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
                <li>
                  <span>Foto 7 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
                <li>
                  <span>Foto 8 -</span>
                  <input type="file" class="input-file" value="Selecionar arquivo" />
                </li>
              </ul>
            </div>
            <div class="grid-cell t60">
              <ul class="list-uploaded">

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <img src="assets/images/user.jpg" alt="Imagem 1" width="150" height="120">
                  <span>1</span>
                </li>

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <!-- <img src="assets/images/user.jpg" alt="Imagem 2" width="150" height="120"> -->
                  <span>2</span>
                </li>

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <!-- <img src="assets/images/user.jpg" alt="Imagem 3" width="150" height="120"> -->
                  <span>3</span>
                </li>

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <!-- <img src="assets/images/user.jpg" alt="Imagem 4" width="150" height="120"> -->
                  <span>4</span>
                </li>

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <!-- <img src="assets/images/user.jpg" alt="Imagem 5" width="150" height="120"> -->
                  <span>5</span>
                </li>

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <!-- <img src="assets/images/user.jpg" alt="Imagem 6" width="150" height="120"> -->
                  <span>6</span>
                </li>

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <!-- <img src="assets/images/user.jpg" alt="Imagem 7" width="150" height="120"> -->
                  <span>7</span>
                </li>

                <li>
                  <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                  <!-- <img src="assets/images/user.jpg" alt="Imagem 8" width="150" height="120"> -->
                  <span>8</span>
                </li>

              </ul>
            </div>
          </div>

        </div>

      </div>
      <div class="button-bar col-top-big">
        <input type="submit" value="Enviar" class="button large center" />
      </div>
    </form>

  </div>

</div>