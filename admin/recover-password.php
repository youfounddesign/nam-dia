<div class="container">
  
  <div class="title-bar">
    <h2>Recuperar senha</h2>
    <p>Digite o e-mail cadastrado para recuperar sua senha.</p>
  </div>

  <form method="post" action="index.php?url=login">

    <div class="row email">
      <label for="email"><i class="icon i25 icon-14"></i></label>
      <input class="input-text" id="email" type="text" placeholder="E-mail">
    </div>

    <div class="button-bar">
      <button class="button large" type="submit">Recuperar</button>
    </div>

  </form>

</div>