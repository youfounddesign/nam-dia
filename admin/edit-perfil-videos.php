<div class="container edit-box">

  <div class="title-bar">
    <h2>Editar perfil</h2>
    <p>Bem vindo! Preencha o formulário e junte-se a esta grande <br>equipe de sucesso.</p>

    <div class="filter">
      <nav>
        <ul class="edit-menu">
          <li><a href="index.php?url=edit-perfil-geral" class="first">Geral</a></li>
          <li><a href="index.php?url=edit-perfil-professional">Profissional</a></li>
          <li><a href="index.php?url=edit-perfil-photos">Fotos</a></li>
          <li><a href="index.php?url=edit-perfil-videos">Vídeos</a></li>
          <li><a href="index.php?url=edit-perfil-curriculo">Currículo</a></li>
        </ul>
      </nav> 
    </div>

  </div>

  <div class="page-content">

    <form method="post" action="index.php?url=perfil-description">
      <div class="content-bar">

        <h3 class="subtitle">Envio<br/> de vídeos</h3>
        
        <p class="instructions">
          <strong>Instruções para o envio de vídeos</strong><br/><br/>
          Para adicionar um video, basta inserir nos campos abaixo uma URL válida do YouTube.<br/>
          Exemplo de URL válida: https://www.youtube.com/watch?v=xj8wHfBKoRU
          <br/><br/>
          <big>OBS: A NaMídia irá analisar os melhores vídeos enviados e postará somente os que tiverem dentro do padrão para o website NaMídia.
          <br/><br/>
          O prazo de postagem é de até 7 dias após o cadastro.</big>
        </p>

        <div class="grid col-top-big">
          <div class="grid-cell t40">
            <ul class="list-upload">
              <li>
                <span>Foto 1 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-1" id="foto-1" />
              </li>
              <li>
                <span>Foto 2 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-2" id="foto-2" />
              </li>
              <li>
                <span>Foto 3 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-3" id="foto-3" />
              </li>
              <li>
                <span>Foto 4 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-4" id="foto-4" />
              </li>
              <li>
                <span>Foto 5 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-5" id="foto-5" />
              </li>
              <li>
                <span>Foto 6 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-6" id="foto-6" />
              </li>
              <li>
                <span>Foto 7 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-7" id="foto-7" />
              </li>
              <li>
                <span>Foto 8 -</span>
                <input type="text" class="input-text" placeholder="Inserir url do youtube" name="foto-8" id="foto-8" />
              </li>
            </ul>
          </div>
          <div class="grid-cell t60">
            <ul class="list-uploaded">

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" />
                <span>1</span>
              </li>

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <!-- <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" /> -->
                <span>2</span>
              </li>

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <!-- <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" /> -->
                <span>3</span>
              </li>

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <!-- <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" /> -->
                <span>4</span>
              </li>

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <!-- <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" /> -->
                <span>5</span>
              </li>

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <!-- <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" /> -->
                <span>6</span>
              </li>

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <!-- <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" /> -->
                <span>7</span>
              </li>

              <li>
                <a href="#" class="delete"><i class="icon i10 icon-2"></i></a>
                <!-- <img src="http://img.youtube.com/vi/XfNrKbSnDwQ/0.jpg" alt="Imagem 1" width="150" height="120" /> -->
                <span>8</span>
              </li>

            </ul>
          </div>
        </div>

      </div>
      <div class="button-bar col-top-big">
        <input type="submit" value="Enviar" class="button large center" />
      </div>
    </form>

  </div>

</div>