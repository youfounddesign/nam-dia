<div class="container">

  <div class="title-bar">
    <h2>Novos usuários</h2>
    <p>Bem vindo administrador! Nesta sessão você poderá<br> conferir todos os cadastros de usuários e gerenciá-los</p>
  </div>

  <div class="page-content">

    <ul class="list-users">

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

      <li>
        <img src="assets/images/user.jpg" alt="Roberto Marques" />
        <span>Roberto de Souza Marques</span>
        <strong>0001</strong>
        <a href="#" class="button">Aprovar</a>
      </li>

    </ul>

    <div class="pagination">
      <ul>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
        <li class="actived"><a href="#"><i class="icon i20 icon-6"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
      </ul>
    </div>

  </div>

</div>