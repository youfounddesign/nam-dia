<div class="container edit-box">

  <div class="title-bar">
    <h2>Editar perfil</h2>
    <p>Bem vindo! Preencha o formulário e junte-se a esta grande <br>equipe de sucesso.</p>

    <div class="filter">
      <nav>
        <ul class="edit-menu">
          <li><a href="index.php?url=edit-perfil-geral" class="first">Geral</a></li>
          <li><a href="index.php?url=edit-perfil-professional">Profissional</a></li>
          <li><a href="index.php?url=edit-perfil-photos">Fotos</a></li>
          <li><a href="index.php?url=edit-perfil-videos">Vídeos</a></li>
          <li><a href="index.php?url=edit-perfil-curriculo">Currículo</a></li>
        </ul>
      </nav> 
    </div>

  </div>

  <div class="page-content">

    <form method="post" action="index.php?url=perfil-description">
      <div class="content-bar">

        <div class="characteristics">
          <h3 class="subtitle">Características</h3>

          <div class="group-row">

            <div class="row r50">
              <label for="nome">Nome artistico</label>
              <input class="input-text" id="nome" type="text" name="nome">
            </div>

            <div class="row r50">
              <label for="sobrenome">Sobre nome</label>
              <input class="input-text" id="sobrenome" type="text" name="sobrenome">
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="nome-artistico">Nome artístico</label>
              <input class="input-text" id="nome-artistico" type="text" name="nome-artistico">
              <span class="help-context">Este será o nome de exibição no seu perfil do website</span>
            </div>

            <div class="row r25">
              <label for="drt">DRT</label>
              <select class="input-text" name="drt" id="drt">
                <option value="nao">Não</option>
                <option value="sim">Sim</option>
              </select>
            </div>

            <div class="row r25">
              <label for="numero-drt">Número DRT</label>
              <input class="input-text" id="numero-drt" type="text" name="numero-drt">
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="nascimento-dia">Data de nascimento</label>

              <select class="input-text" name="nascimento-dia" id="nascimento-dia" style="float: left; width: 118px;margin-right: 25px;">
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
              </select>

              <select class="input-text" name="nascimento-mes" id="nascimento-mes" style="float: left; width: 118px;margin-right: 23px;">
                <option value="01">Janeiro</option>
                <option value="02">Fevereiro</option>
                <option value="03">Março</option>
                <option value="04">Abril</option>
                <option value="05">Maio</option>
                <option value="06">Junho</option>
                <option value="07">Julho</option>
                <option value="08">Agosto</option>
                <option value="09">Setembro</option>
                <option value="10">Outubro</option>
                <option value="11">Novembro</option>
                <option value="12">Dezembro</option>
              </select>

              <select class="input-text" name="nascimento-ano" id="nascimento-ano" style="float: left; width: 118px;margin-right: 23px;">
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
                <option value="1999">1999</option>
                <option value="1998">1998</option>
                <option value="1997">1997</option>
                <option value="1996">1996</option>
                <option value="1995">1995</option>
                <option value="1994">1994</option>
                <option value="1993">1993</option>
                <option value="1992">1992</option>
                <option value="1991">1991</option>
                <option value="1990">1990</option>
                <option value="1989">1989</option>
                <option value="1988">1988</option>
                <option value="1987">1987</option>
                <option value="1986">1986</option>
                <option value="1985">1985</option>
                <option value="1984">1984</option>
                <option value="1983">1983</option>
                <option value="1982">1982</option>
                <option value="1981">1981</option>
                <option value="1980">1980</option>
                <option value="1979">1979</option>
                <option value="1978">1978</option>
                <option value="1977">1977</option>
                <option value="1976">1976</option>
                <option value="1975">1975</option>
                <option value="1974">1974</option>
                <option value="1973">1973</option>
                <option value="1972">1972</option>
                <option value="1971">1971</option>
                <option value="1970">1970</option>
                <option value="1969">1969</option>
                <option value="1968">1968</option>
                <option value="1967">1967</option>
                <option value="1966">1966</option>
                <option value="1965">1965</option>
                <option value="1964">1964</option>
                <option value="1963">1963</option>
                <option value="1962">1962</option>
                <option value="1961">1961</option>
                <option value="1960">1960</option>
                <option value="1959">1959</option>
                <option value="1958">1958</option>
                <option value="1957">1957</option>
                <option value="1956">1956</option>
                <option value="1955">1955</option>
                <option value="1954">1954</option>
                <option value="1953">1953</option>
                <option value="1952">1952</option>
                <option value="1951">1951</option>
                <option value="1950">1950</option>
                <option value="1949">1949</option>
                <option value="1948">1948</option>
                <option value="1947">1947</option>
                <option value="1946">1946</option>
                <option value="1945">1945</option>
                <option value="1944">1944</option>
                <option value="1943">1943</option>
                <option value="1942">1942</option>
                <option value="1941">1941</option>
                <option value="1940">1940</option>
                <option value="1939">1939</option>
                <option value="1938">1938</option>
                <option value="1937">1937</option>
                <option value="1936">1936</option>
                <option value="1935">1935</option>
                <option value="1934">1934</option>
                <option value="1933">1933</option>
                <option value="1932">1932</option>
                <option value="1931">1931</option>
                <option value="1930">1930</option>
              </select>

            </div>

            <div class="row r50">
              <label for="rg">RG</label>
              <input class="input-text" id="rg" type="text" name="rg" rg>
            </div>

          </div>

          <div class="group-row">

            <div class="row r25">
              <label for="cnh">CNH</label>
              <select class="input-text" name="cnh" id="cnh" style="float: left; width: 118px;margin-right: 25px;">
                <option value="nao">Não</option>
                <option value="sim">Sim</option>
              </select>
            </div>

            <div class="row r25">
              <label for="sexo">Sexo</label>
              <select class="input-text" name="sexo" id="sexo" style="float: left; width: 118px;margin-right: 25px;">
                <option value="masculino">Masculino</option>
                <option value="feminino">Feminino</option>
              </select>
            </div>

            <div class="row r50">
              <label for="feminino">CPF</label>
              <input class="input-text" id="feminino" type="text" name="feminino" cpf>
            </div>

          </div>

          <div class="group-row">

            <div class="row r25">
              <label for="altura">Altura</label>
              <input class="input-text" id="altura" type="text" name="altura">
            </div>

            <div class="row r25">
              <label for="peso">Peso</label>
              <input class="input-text" id="peso" type="text" name="peso">
            </div>

            <div class="row r25">
              <label for="manequim">Manequim</label>
              <input class="input-text" id="manequim" type="text" name="manequim">
            </div>

            <div class="row r25">
              <label for="quadril">Quadril</label>
              <input class="input-text" id="quadril" type="text" name="quadril">
            </div>

          </div>

          <div class="group-row">

            <div class="row r25">
              <label for="busto">Busto</label>
              <input class="input-text" id="busto" type="text" name="busto">
            </div>

            <div class="row r25">
              <label for="cintura">Cintura</label>
              <input class="input-text" id="cintura" type="text" name="cintura">
            </div>

            <div class="row r25">
              <label for="calcado">Calçado</label>
              <input class="input-text" id="calcado" type="text" name="calcado">
            </div>

            <div class="row r25">
              <label for="cor-pele">Cor da pele</label>
              <select class="input-text" name="cor-pele" id="cor-pele">
                <option value="negra">Negra</option>
                <option value="media">Média</option>
                <option value="Clara">Clara</option>
                <option value="muito-clara">Muito clara</option>
              </select>
            </div>

          </div>

          <div class="group-row">

            <div class="row r25">
              <label for="cor-olhos">Cor dos olhos</label>
              <select class="input-text" name="cor-olhos" id="cor-olhos" style="float: left; width: 118px;">
                <option value="castanho">Castanho</option>
                <option value="azul">Azul</option>
                <option value="verde">Verde</option>
              </select>
            </div>

            <div class="row r25">
              <label for="cor-cabelo">Cor do cabelo</label>
              <select class="input-text" name="cor-cabelo" id="cor-cabelo" style="float: left; width: 118px;">
                <option value="castanho">Castanho</option>
                <option value="preto">Preto</option>
                <option value="loiro">Loiro</option>
              </select>
            </div>

            <div class="row r50">
              <label for="cor-cabelo">Tipo do cabelo</label>
              <select class="input-text" name="cor-cabelo" id="cor-cabelo" style="float: left; width: 404px;">
                <option value="encaracolados">Encaracolados</option>
                <option value="liso">Liso</option>
              </select>
            </div>

          </div>

          <div class="group-row">

            <div class="row r100">

              <label>Detalhes</label>

              <div class="check-button-list" style="width: 315px;">

                <div class="check-button">
                  <label for="tatuagem">
                    <input type="checkbox" name="detalhes" value="tatuagem" id="tatuagem" />
                    <i class="icon i20 icon-8"></i>
                    <span>Tatuagem</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="barba">
                    <input type="checkbox" name="detalhes" value="barba" id="barba" />
                    <i class="icon i20 icon-8"></i>
                    <span>Barba</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="bigode">
                    <input type="checkbox" name="detalhes" value="bigode" id="bigode" />
                    <i class="icon i20 icon-8"></i>
                    <span>Bigode</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="pierging">
                    <input type="checkbox" name="detalhes" value="pierging" id="pierging" />
                    <i class="icon i20 icon-8"></i>
                    <span>Pierging</span>
                  </label>
                </div>

            </div>

          </div>

        </div>

        <div class="contacts col-top-big">
          <h3 class="subtitle">Contatos</h3>

          <div class="group-row">

            <div class="row r25">
              <label for="tel1">Telefone 1</label>
              <input class="input-text" id="tel1" type="text" name="tel1" phone>
            </div>

            <div class="row r25">
              <label for="tel2">Telefone 2</label>
              <input class="input-text" id="tel2" type="text" name="tel2" phone>
            </div>

            <div class="row r25">
              <label for="cel1">Celular 1</label>
              <input class="input-text" id="cel1" type="text" name="cel1" phone>
            </div>

            <div class="row r25">
              <label for="cel2">Celular 2</label>
              <input class="input-text" id="cel2" type="text" name="cel2" phone>
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="site">Site</label>
              <input class="input-text" id="site" type="text" name="site">
            </div>

            <div class="row r50">
              <label for="facebook">Facebook</label>
              <input class="input-text" id="facebook" type="text" name="facebook">
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="twitter">Twitter</label>
              <input class="input-text" id="twitter" type="text" name="twitter">
            </div>

            <div class="row r50">
              <label for="instagram">Instagram</label>
              <input class="input-text" id="instagram" type="text" name="instagram">
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="google">Google +</label>
              <input class="input-text" id="google" type="text" name="google">
            </div>

            <div class="row r50">
              <label for="youtube">Youtube</label>
              <input class="input-text" id="youtube" type="text" name="youtube">
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="linkedin">Linkedin</label>
              <input class="input-text" id="linkedin" type="text" name="linkedin">
            </div>

          </div>

        </div>

        <div class="contacts col-top-big">
          <h3 class="subtitle">Endereço</h3>

          <div class="group-row">

            <div class="row r50">
              <label for="endereco">Endereço</label>
              <input class="input-text" id="endereco" type="text" name="endereco">
            </div>

            <div class="row r25">
              <label for="cep">CEP</label>
              <input class="input-text" id="cep" type="text" name="cep" cep>
            </div>

            <div class="row r25">
              <label for="pais">País</label>
              <select class="input-text" name="pais" id="pais">
                <option value="África do Sul">África do Sul</option>
                <option value="Albânia">Albânia</option>
                <option value="Alemanha">Alemanha</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antigua">Antigua</option>
                <option value="Arábia Saudita">Arábia Saudita</option>
                <option value="Argentina">Argentina</option>
                <option value="Armênia">Armênia</option>
                <option value="Aruba">Aruba</option>
                <option value="Austrália">Austrália</option>
                <option value="Áustria">Áustria</option>
                <option value="Azerbaijão">Azerbaijão</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrein">Bahrein</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Bélgica">Bélgica</option>
                <option value="Benin">Benin</option>
                <option value="Bermudas">Bermudas</option>
                <option value="Botsuana">Botsuana</option>
                <option value="Brasil" selected>Brasil</option>
                <option value="Brunei">Brunei</option>
                <option value="Bulgária">Bulgária</option>
                <option value="Burkina Fasso">Burkina Fasso</option>
                <option value="botão">botão</option>
                <option value="Cabo Verde">Cabo Verde</option>
                <option value="Camarões">Camarões</option>
                <option value="Camboja">Camboja</option>
                <option value="Canadá">Canadá</option>
                <option value="Cazaquistão">Cazaquistão</option>
                <option value="Chade">Chade</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Cidade do Vaticano">Cidade do Vaticano</option>
                <option value="Colômbia">Colômbia</option>
                <option value="Congo">Congo</option>
                <option value="Coréia do Sul">Coréia do Sul</option>
                <option value="Costa do Marfim">Costa do Marfim</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Croácia">Croácia</option>
                <option value="Dinamarca">Dinamarca</option>
                <option value="Djibuti">Djibuti</option>
                <option value="Dominica">Dominica</option>
                <option value="EUA">EUA</option>
                <option value="Egito">Egito</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Emirados Árabes">Emirados Árabes</option>
                <option value="Equador">Equador</option>
                <option value="Eritréia">Eritréia</option>
                <option value="Escócia">Escócia</option>
                <option value="Eslováquia">Eslováquia</option>
                <option value="Eslovênia">Eslovênia</option>
                <option value="Espanha">Espanha</option>
                <option value="Estônia">Estônia</option>
                <option value="Etiópia">Etiópia</option>
                <option value="Fiji">Fiji</option>
                <option value="Filipinas">Filipinas</option>
                <option value="Finlândia">Finlândia</option>
                <option value="França">França</option>
                <option value="Gabão">Gabão</option>
                <option value="Gâmbia">Gâmbia</option>
                <option value="Gana">Gana</option>
                <option value="Geórgia">Geórgia</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Granada">Granada</option>
                <option value="Grécia">Grécia</option>
                <option value="Guadalupe">Guadalupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guiana">Guiana</option>
                <option value="Guiana Francesa">Guiana Francesa</option>
                <option value="Guiné-bissau">Guiné-bissau</option>
                <option value="Haiti">Haiti</option>
                <option value="Holanda">Holanda</option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungria">Hungria</option>
                <option value="Iêmen">Iêmen</option>
                <option value="Ilhas Cayman">Ilhas Cayman</option>
                <option value="Ilhas Cook">Ilhas Cook</option>
                <option value="Ilhas Curaçao">Ilhas Curaçao</option>
                <option value="Ilhas Marshall">Ilhas Marshall</option>
                <option value="Ilhas Turks & Caicos">Ilhas Turks & Caicos</option>
                <option value="Ilhas Virgens (brit.)">Ilhas Virgens (brit.)</option>
                <option value="Ilhas Virgens(amer.)">Ilhas Virgens(amer.)</option>
                <option value="Ilhas Wallis e Futuna">Ilhas Wallis e Futuna</option>
                <option value="Índia">Índia</option>
                <option value="Indonésia">Indonésia</option>
                <option value="Inglaterra">Inglaterra</option>
                <option value="Irlanda">Irlanda</option>
                <option value="Islândia">Islândia</option>
                <option value="Israel">Israel</option>
                <option value="Itália">Itália</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japão">Japão</option>
                <option value="Jordânia">Jordânia</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Latvia">Latvia</option>
                <option value="Líbano">Líbano</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lituânia">Lituânia</option>
                <option value="Luxemburgo">Luxemburgo</option>
                <option value="Macau">Macau</option>
                <option value="Macedônia">Macedônia</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malásia">Malásia</option>
                <option value="Malaui">Malaui</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marrocos">Marrocos</option>
                <option value="Martinica">Martinica</option>
                <option value="Mauritânia">Mauritânia</option>
                <option value="Mauritius">Mauritius</option>
                <option value="México">México</option>
                <option value="Moldova">Moldova</option>
                <option value="Mônaco">Mônaco</option>
                <option value="Montserrat">Montserrat</option>
                <option value="Nepal">Nepal</option>
                <option value="Nicarágua">Nicarágua</option>
                <option value="Niger">Niger</option>
                <option value="Nigéria">Nigéria</option>
                <option value="Noruega">Noruega</option>
                <option value="Nova Caledônia">Nova Caledônia</option>
                <option value="Nova Zelândia">Nova Zelândia</option>
                <option value="Omã">Omã</option>
                <option value="Palau">Palau</option>
                <option value="Panamá">Panamá</option>
                <option value="Papua-nova Guiné">Papua-nova Guiné</option>
                <option value="Paquistão">Paquistão</option>
                <option value="Peru">Peru</option>
                <option value="Polinésia Francesa">Polinésia Francesa</option>
                <option value="Polônia">Polônia</option>
                <option value="Porto Rico">Porto Rico</option>
                <option value="Portugal">Portugal</option>
                <option value="Qatar">Qatar</option>
                <option value="Quênia">Quênia</option>
                <option value="Rep. Dominicana">Rep. Dominicana</option>
                <option value="Rep. Tcheca">Rep. Tcheca</option>
                <option value="Reunion">Reunion</option>
                <option value="Romênia">Romênia</option>
                <option value="Ruanda">Ruanda</option>
                <option value="Rússia">Rússia</option>
                <option value="Saipan">Saipan</option>
                <option value="Samoa Americana">Samoa Americana</option>
                <option value="Senegal">Senegal</option>
                <option value="Serra Leone">Serra Leone</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Singapura">Singapura</option>
                <option value="Síria">Síria</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="St. Kitts & Nevis">St. Kitts & Nevis</option>
                <option value="St. Lúcia">St. Lúcia</option>
                <option value="St. Vincent">St. Vincent</option>
                <option value="Sudão">Sudão</option>
                <option value="Suécia">Suécia</option>
                <option value="Suiça">Suiça</option>
                <option value="Suriname">Suriname</option>
                <option value="Tailândia">Tailândia</option>
                <option value="Taiwan">Taiwan</option>
                <option value="Tanzânia">Tanzânia</option>
                <option value="Togo">Togo</option>
                <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                <option value="Tunísia">Tunísia</option>
                <option value="Turquia">Turquia</option>
                <option value="Ucrânia">Ucrânia</option>
                <option value="Uganda">Uganda</option>
                <option value="Uruguai">Uruguai</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Vietnã">Vietnã</option>
                <option value="Zaire">Zaire</option>
                <option value="Zâmbia">Zâmbia</option>
                <option value="Zimbábue">Zimbábue</option>
              </select>
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="estado">Estado</label>
              <input class="input-text" id="estado" type="text" name="estado">
            </div>

            <div class="row r50">
              <label for="cidade">Cidade</label>
              <input class="input-text" id="cidade" type="text" name="cidade">
            </div>

          </div>

        </div>

        <div class="contacts col-top-big">
          <h3 class="subtitle">Dados<br> Bancários</h3>

          <div class="group-row">

            <div class="row r25">
              <label for="banco">Banco</label>
              <input class="input-text" id="banco" type="text" name="banco">
            </div>

            <div class="row r25">
              <label for="tipo-conta">Tipo de conta</label>
              <select class="input-text" name="tipo-conta" id="tipo-conta">
                <option value="Corrente">Corrente</option>
                <option value="Poupança">Poupança</option>
              </select>
            </div>

            <div class="row r50">
              <label for="favorecido">Favorecido</label>
              <input class="input-text" id="favorecido" type="text" name="favorecido">
            </div>

          </div>

          <div class="group-row">

            <div class="row r25">
              <label for="agencia">Agência</label>
              <input class="input-text" id="agencia" type="text" name="agencia">
            </div>

            <div class="row r25">
              <label for="conta">Conta</label>
              <input class="input-text" id="conta" type="text" name="conta">
            </div>

          </div>

        </div>

      </div>
      <div class="button-bar col-top-big">
        <input type="submit" value="Salvar dados" class="button large center" />
      </div>
    </form>

  </div>

</div>