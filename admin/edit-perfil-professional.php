<div class="container edit-box">

  <div class="title-bar">
    <h2>Editar perfil</h2>
    <p>Bem vindo! Preencha o formulário e junte-se a esta grande <br>equipe de sucesso.</p>

    <div class="filter">
      <nav>
        <ul class="edit-menu">
          <li><a href="index.php?url=edit-perfil-geral" class="first">Geral</a></li>
          <li><a href="index.php?url=edit-perfil-professional">Profissional</a></li>
          <li><a href="index.php?url=edit-perfil-photos">Fotos</a></li>
          <li><a href="index.php?url=edit-perfil-videos">Vídeos</a></li>
          <li><a href="index.php?url=edit-perfil-curriculo">Currículo</a></li>
        </ul>
      </nav> 
    </div>

  </div>

  <div class="page-content">

    <div class="content-bar">

      <form method="post" action="index.php?url=perfil-description">

        <div class="categories">
          <h3 class="subtitle">Categorias</h3>
          <p class="text-white col-bottom-small">selecione o tipo de perfil em que você se enquadra como profissional.</p>

          <div class="group-row">

            <div class="row r100">
              <div class="check-button-list">

                <div class="check-button">
                  <label for="categoria-ator">
                    <input type="checkbox" name="ator" value="ator" id="categoria-ator" />
                    <i class="icon i20 icon-8"></i>
                    <span>Ator</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="categoria-atriz">
                    <input type="checkbox" name="atriz" value="atriz" id="categoria-atriz" />
                    <i class="icon i20 icon-8"></i>
                    <span>Atriz</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="categoria-promotor">
                    <input type="checkbox" name="promotor" value="promotor" id="categoria-promotor" />
                    <i class="icon i20 icon-8"></i>
                    <span>Promotor</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="categoria-artista">
                    <input type="checkbox" name="artista" value="artista" id="categoria-artista" />
                    <i class="icon i20 icon-8"></i>
                    <span>Artista</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="categoria-modelo">
                    <input type="checkbox" name="modelo" value="modelo" id="categoria-modelo" />
                    <i class="icon i20 icon-8"></i>
                    <span>Modelo</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="categoria-figurante">
                    <input type="checkbox" name="figurante" value="figurante" id="categoria-figurante" />
                    <i class="icon i20 icon-8"></i>
                    <span>Figurante</span>
                  </label>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="formation col-top-big">
          <h3 class="subtitle">Formação</h3>

          <div class="group-row">

            <div class="row r25">
              <label for="Escolaridade">Escolaridade</label>
              <select class="input-text" name="escolaridade" id="escolaridade">
                <option value="Nivel Medio (2º grau)">Nivel Medio (2º grau)</option>
                <option value="Superior">Superior</option>
                <option value="Pós-graduação">Pós-graduação</option>
                <option value="Mestrado">Mestrado</option>
                <option value="Doutorado">Doutorado</option>
                <option value="Pós-doutorado">Pós-doutorado</option>
              </select>
            </div>

            <div class="row r25">
              <label for="situacao">Situação</label>
              <select class="input-text" name="situacao" id="situacao">
                <option value="Completo">Completo</option>
                <option value="Incompleto">Incompleto</option>
                <option value="Em andamento">Em andamento</option>
              </select>
            </div>

            <div class="row r25">
              <label for="ingles">Inglês</label>
              <select class="input-text" name="ingles" id="ingles">
                <option value="basico">Básico</option>
                <option value="intermediario">Intermediario</option>
                <option value="avancado">Avançado</option>
              </select>
            </div>

            <div class="row r25">
              <label for="espanhol">Espanhol</label>
              <select class="input-text" name="espanhol" id="espanhol">
                <option value="basico">Básico</option>
                <option value="intermediario">Intermediario</option>
                <option value="avancado">Avançado</option>
              </select>
            </div>

          </div>

          <div class="group-row">

            <div class="row r25">
              <label for="frances">Francês</label>
              <select class="input-text" name="frances" id="frances">
                <option value="basico">Básico</option>
                <option value="intermediario">Intermediario</option>
                <option value="avancado">Avançado</option>
              </select>
            </div>

            <div class="row r25">
              <label for="outro">Outro idioma</label>
              <input class="input-text" id="outro" type="text" name="outro">
            </div>

            <div class="row r25">
              <label for="windows">Windows</label>
              <select class="input-text" name="windows" id="windows">
                <option value="basico">Básico</option>
                <option value="intermediario">Intermediario</option>
                <option value="avancado">Avançado</option>
              </select>
            </div>

            <div class="row r25">
              <label for="word">Word</label>
              <select class="input-text" name="word" id="word">
                <option value="basico">Básico</option>
                <option value="intermediario">Intermediario</option>
                <option value="avancado">Avançado</option>
              </select>
            </div>

          </div>

          <div class="group-row">

            <div class="row r25">
              <label for="excel">Excel</label>
              <select class="input-text" name="excel" id="excel">
                <option value="basico">Básico</option>
                <option value="intermediario">Intermediario</option>
                <option value="avancado">Avançado</option>
              </select>
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="descricao-carreira">Descrição prévia sobre sua carreira</label>
              <textarea class="input-text" name="descricao-carreira" id="descricao-carreira"></textarea>
              <span class="help-context">Max. 300 caracteres</span>
            </div>

            <div class="row r50">
              <label for="experiencia-profissional">Experiência profissional</label>
              <textarea class="input-text" name="experiencia-profissional" id="experiencia-profissional"></textarea>
              <span class="help-context">Max. 300 caracteres</span>
            </div>

          </div>

        </div>

        <div class="atividades col-top-big">
          <h3 class="subtitle">Atividades<br/> especiais</h3>

          <div class="group-row">

            <div class="row r50">
              <label for="artes-marciais">Artes marciais</label>
              <textarea class="input-text" name="artes-marciais" id="artes-marciais"></textarea>
            </div>

            <div class="row r50">
              <label for="artes-circenses">Artes circenses</label>
              <textarea class="input-text" name="artes-circenses" id="artes-circenses"></textarea>
            </div>

          </div>

          <div class="group-row">

            <div class="row r50">
              <label for="esportes">Esportes</label>
              <textarea class="input-text" name="esportes" id="esportes"></textarea>
            </div>

            <div class="row r50">
              <label for="musica">Música</label>
              <textarea class="input-text" name="musica" id="musica"></textarea>
            </div>

          </div>
        </div>

        <div class="trabalhos col-top-big">
          <h3 class="subtitle">Trabalhos com<br/> figuração</h3>

          <div class="group-row">

            <div class="row r50">
              <label for="figuracao">Figuração</label>
              <textarea class="input-text" name="figuracao" id="figuracao"></textarea>
            </div>

          </div>
        </div>

        <div class="trabalhos-modelo col-top-big">
          <h3 class="subtitle">Trabalhos como<br/> modelo</h3>

          <div class="group-row">

            <div class="row r100">
              <div class="check-button-list large">

                <div class="check-button">
                  <label for="fotos-para-catalogo">
                    <input type="checkbox" name="fotos-para-catalogo" value="fotos-para-catalogo" id="fotos-para-catalogo" />
                    <i class="icon i20 icon-8"></i>
                    <span>Fotos para catálogo</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="semi-nu">
                    <input type="checkbox" name="semi-nu" value="semi-nu" id="semi-nu" />
                    <i class="icon i20 icon-8"></i>
                    <span>Semi-nú</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="ensaios-sensuais">
                    <input type="checkbox" name="ensaios-sensuais" value="ensaios-sensuais" id="ensaios-sensuais" />
                    <i class="icon i20 icon-8"></i>
                    <span>Ensaios sensuais</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="modelo-fotografico">
                    <input type="checkbox" name="modelo-fotografico" value="modelo-fotografico" id="modelo-fotografico" />
                    <i class="icon i20 icon-8"></i>
                    <span>Modelo fotográfico</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="nu-artistico">
                    <input type="checkbox" name="nu-artistico" value="nu-artistico" id="nu-artistico" />
                    <i class="icon i20 icon-8"></i>
                    <span>Nú artistico</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="pintura-corporal">
                    <input type="checkbox" name="pintura-corporal" value="pintura-corporal" id="pintura-corporal" />
                    <i class="icon i20 icon-8"></i>
                    <span>Pintura corporal</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="modelo-passarela">
                    <input type="checkbox" name="modelo-passarela" value="modelo-passarela" id="modelo-passarela" />
                    <i class="icon i20 icon-8"></i>
                    <span>Modelo passarela</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="manequim">
                    <input type="checkbox" name="manequim" value="manequim" id="manequim" />
                    <i class="icon i20 icon-8"></i>
                    <span>Manequim</span>
                  </label>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="trabalhos-publicitarios col-top-big">
          <h3 class="subtitle">Trabalhos<br/> publicitários</h3>

          <div class="group-row">

            <div class="row r100">
              <div class="check-button-list large">

                <div class="check-button">
                  <label for="figurante">
                    <input type="checkbox" name="figurante" value="figurante" id="figurante" />
                    <i class="icon i20 icon-8"></i>
                    <span>Figurante</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="coadjuvante">
                    <input type="checkbox" name="coadjuvante" value="coadjuvante" id="coadjuvante" />
                    <i class="icon i20 icon-8"></i>
                    <span>Coadjuvante</span>
                  </label>
                </div>

                <div class="check-button" style="width: 230px;">
                  <label for="ator-atriz-interprete">
                    <input type="checkbox" name="ator-atriz-interprete" value="ator-atriz-interprete" id="ator-atriz-interprete" />
                    <i class="icon i20 icon-8"></i>
                    <span>Ator / Atriz / Intérprete</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="cantor">
                    <input type="checkbox" name="cantor" value="cantor" id="cantor" />
                    <i class="icon i20 icon-8"></i>
                    <span>Cantor</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="dublador">
                    <input type="checkbox" name="dublador" value="dublador" id="dublador" />
                    <i class="icon i20 icon-8"></i>
                    <span>Dublador</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="locutor">
                    <input type="checkbox" name="locutor" value="locutor" id="locutor" />
                    <i class="icon i20 icon-8"></i>
                    <span>Locutor</span>
                  </label>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="trabalhos-publicitarios col-top-big">
          <h3 class="subtitle">Trabalhos<br/> em eventos</h3>

          <div class="group-row">

            <div class="row r100">
              <div class="check-button-list large">

                <div class="check-button">
                  <label for="recepcionista">
                    <input type="checkbox" name="recepcionista" value="recepcionista" id="recepcionista" />
                    <i class="icon i20 icon-8"></i>
                    <span>recepcionista</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="promotor">
                    <input type="checkbox" name="promotor" value="promotor" id="promotor" />
                    <i class="icon i20 icon-8"></i>
                    <span>promotor</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="dancarino">
                    <input type="checkbox" name="dancarino" value="dancarino" id="dancarino" />
                    <i class="icon i20 icon-8"></i>
                    <span>dançarino</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="hostess">
                    <input type="checkbox" name="hostess" value="hostess" id="hostess" />
                    <i class="icon i20 icon-8"></i>
                    <span>hostess</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="supervisor">
                    <input type="checkbox" name="supervisor" value="supervisor" id="supervisor" />
                    <i class="icon i20 icon-8"></i>
                    <span>supervisor(a)</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="pdv">
                    <input type="checkbox" name="pdv" value="pdv" id="pdv" />
                    <i class="icon i20 icon-8"></i>
                    <span>pdv</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="abordagem">
                    <input type="checkbox" name="abordagem" value="abordagem" id="abordagem" />
                    <i class="icon i20 icon-8"></i>
                    <span>abordagem</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="recreador">
                    <input type="checkbox" name="recreador" value="recreador" id="recreador" />
                    <i class="icon i20 icon-8"></i>
                    <span>recreador(a)</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="degustador">
                    <input type="checkbox" name="degustador" value="degustador" id="degustador" />
                    <i class="icon i20 icon-8"></i>
                    <span>degustador(a)</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="manuseio-de-brindes">
                    <input type="checkbox" name="manuseio-de-brindes" value="manuseio-de-brindes" id="manuseio-de-brindes" />
                    <i class="icon i20 icon-8"></i>
                    <span>manuseio de brindes</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="runner">
                    <input type="checkbox" name="runner" value="runner" id="runner" />
                    <i class="icon i20 icon-8"></i>
                    <span>runner</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="garcom-garconete">
                    <input type="checkbox" name="garcom-garconete" value="garcom-garconete" id="garcom-garconete" />
                    <i class="icon i20 icon-8"></i>
                    <span>garçom / garçonete</span>
                  </label>
                </div>

                <div class="check-button">
                  <label for="administrador-de-caixa">
                    <input type="checkbox" name="administrador-de-caixa" value="administrador-de-caixa" id="administrador-de-caixa" />
                    <i class="icon i20 icon-8"></i>
                    <span>administrador(a) de caixa</span>
                  </label>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="button-bar col-top-big">
          <input type="submit" value="Salvar dados" class="button large center" />
        </div>

    </form>

  </div>

</div>