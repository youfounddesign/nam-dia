<div class="container">

  <div class="title-bar">
    <h2>Alterar senha</h2>
    <p>Digite sua senha e nova senha para alterar o acesso ao sistema.</p>
  </div>

  <form method="post" action="index.php?url=login">

    <div class="row current-password">
      <label for="current-password"><i class="icon i25 icon-15"></i></label>
      <input class="input-text" id="current-password" type="password" placeholder="Senha atual">
    </div>

    <div class="row new-password">
      <label for="new-password"><i class="icon i25 icon-15"></i></label>
      <input class="input-text" id="new-password" type="password" placeholder="Nova senha">
    </div>

    <div class="row new-password-again">
      <label for="new-password-again"><i class="icon i25 icon-15"></i></label>
      <input class="input-text" id="new-password-again" type="password" placeholder="Confirmar nova senha">
    </div>

    <div class="button-bar">
      <button class="button large" type="submit">Alterar</button>
    </div>

  </form>

</div>