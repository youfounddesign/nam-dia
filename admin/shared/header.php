<header class="header">
  <div class="container">

    <div class="grid">

      <div class="grid-cell t50">

        <a href="index.php?url=users" class="logo">
          <img src="../assets/images/logo-namidia.png" alt="Namídia" />
        </a>

      </div>

      <div class="grid-cell t50">
        <div class="logout">
          <a href="index.php?url=login"><i class="icon i20 icon-7"></i></a>
        </div>
        <nav>

          <!-- Menu admministrador -->
          <ul class="admin-menu">
            <li><a href="index.php?url=users">Usuários</a></li>
            <li><a href="index.php?url=new-users">Novos usuários <span class="tag">25</span></a></li>
          </ul>
          <!-- end -->

          <!-- Menu usuários
          <ul class="admin-menu">
            <li><a href="index.php?url=perfil-description">Perfil</a></li>
            <li><a href="index.php?url=edit-perfil-description">Editar perfil</a></li>
            <li><a href="index.php?url=edit-password">Alterar senha</a></li>
          </ul>
          end -->

        </nav>
      </div>

    </div>
  </div>
</header>