<div class="container">

  <div class="title-bar">
    <h2>Login</h2>
    <p>Entre e confira ou edite seu cadastro e perfil no website Namídia</p>
  </div>

  <form method="post" action="index.php?url=users">

    <div class="row email">
      <label for="email"><i class="icon i25 icon-14"></i></label>
      <input class="input-text" tabindex="1" id="email" type="text" placeholder="E-mail">
    </div>

    <div class="row password">
      <label for="senha"><i class="icon i25 icon-16"></i></label>
      <input class="input-text" id="senha" tabindex="2" type="password" placeholder="Senha">
    </div>

    <a href="index.php?url=recover-password" class="recover-password">Esqueceu sua senha?</a>

    <div class="button-bar">
      <button class="button large" tabindex="3" type="submit">Acessar</button>
    </div>

  </form>

</div>

<script type="text/javascript">
  $(function() {
    setTimeout(function() {
      $('.content').find('.input-text:first').focus();
    }, 100);
  });
</script>