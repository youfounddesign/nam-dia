<div class="container">

  <div class="title-bar">
    <h2>Usuários</h2>
    <p>Bem vindo administrador! Nesta sessão você poderá<br> conferir todos os cadastros de usuários e gerenciá-los</p>
  </div>

  <form class="search-bar" method="get" action="index.php?url=users">

    <div class="row email">
      <input class="input-text small" id="name" type="text" placeholder="Nome">
    </div>

    <div class="row registration">
      <input class="input-text small" id="registration" type="text" placeholder="Matrícula">
    </div>

    <div class="button-bar">
      <button class="button small" type="submit">Buscar</button>
    </div>

  </form>

  <div class="page-content">

    <ul class="list-users">

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0001</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0002</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0003</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0004</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0005</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0006</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0007</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0008</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0005</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0006</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0007</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0008</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0005</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0006</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0007</strong>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/user.jpg" alt="Roberto Marques" />
          <span>Roberto de Souza Marques</span>
          <strong>0008</strong>
        </a>
      </li>

    </ul>

    <div class="pagination">
      <ul>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
        <li class="actived"><a href="#"><i class="icon i20 icon-6"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-5"></i></a></li>
      </ul>
    </div>

  </div>

</div>