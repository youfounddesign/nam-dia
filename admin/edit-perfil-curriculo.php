<div class="container edit-box">

  <div class="title-bar">
    <h2>Editar perfil</h2>
    <p>Bem vindo! Preencha o formulário e junte-se a esta grande <br>equipe de sucesso.</p>

    <div class="filter">
      <nav>
        <ul class="edit-menu">
          <li><a href="index.php?url=edit-perfil-geral" class="first">Geral</a></li>
          <li><a href="index.php?url=edit-perfil-professional">Profissional</a></li>
          <li><a href="index.php?url=edit-perfil-photos">Fotos</a></li>
          <li><a href="index.php?url=edit-perfil-videos">Vídeos</a></li>
          <li><a href="index.php?url=edit-perfil-curriculo">Currículo</a></li>
        </ul>
      </nav> 
    </div>

  </div>

  <div class="page-content curriculo-admin">

    <form method="post" action="index.php?url=perfil-description">
      <div class="content-bar">

        <h3 class="subtitle">Currículo</h3>
        
        <div class="group-row">

          <div class="row r25">
            <img src="../assets/images/im-ampty-user.jpg" alt="Nenhuma foto" />
          </div>

          <div class="row r50 last">
            <label for="foto">Foto -</label>
            <input type="file" class="input-file" value="Selecionar arquivo" />
          </div>

        </div>

        <div class="group-row">

          <div class="row r75">
            <label for="curriculo">Seu currículo</label>
            <textarea class="input-text" name="curriculo" id="curriculo" style="min-height: 300px;"></textarea>
          </div>

        </div>

      </div>
      <div class="button-bar col-top-big">
        <input type="submit" value="Enviar" class="button large center" />
      </div>
    </form>

  </div>

</div>