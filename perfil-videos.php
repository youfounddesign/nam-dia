<? include 'shared/banner.php' ?>

<div class="container">
  <div class="page-content">

    <div class="grid top-bar">
      <div class="grid-cell t65">
        <h3 class="subtitle">Roberto<br> Marques</h3>
      </div>
      <div class="grid-cell t35">
        <nav class="internal-navigation">
          <ul class="tabs-action">
            <li><a href="index.php?url=perfil-description">Fotos e descrição</a></li>
            <li><a href="index.php?url=perfil-videos">Vídeos</a></li>
            <!-- <li><a href="index.php?url=perfil-imprensa">Imprensa</a></li> -->
            <li><a href="index.php?url=perfil-curriculo">Currículo</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="content-bar">

      <!-- Se a informação estiver vazia, mostrar esta div
        <div class="empty">
          <p>Clique em editar para preencher os dados do seu perfil.</p>
        </div>
      end -->

      <ul class="list-videos">
        <?
          // Pegue a url que o admin colocará no sistema e jogue nessa variavel. Faça um while aqui para mostrar a lista de imagens com seus respectivos vídeos
          $link = 'https://www.youtube.com/watch?v=XfNrKbSnDwQ'; // variavel com a url do vídeo
          preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $link , $saida); // expressão regular que tira o id do vídeo

          // while falso
          $i = 0;
          while( $i <= 7 ){

            // imagem do vídeo com a variável que contem somente o id. Com isso, ele gera a imagem
            echo '
              <li>
                <a href="http://www.youtube.com/embed/'. $saida[0] .'?autoplay=1" class="expand fancybox.iframe">
                  <i class="icon i80 icon-1"></i>
                  <img src="http://img.youtube.com/vi/'. $saida[0] .'/0.jpg" />
                </a>
              </li>
            ';
            $i++;
          }
        ?>
      </ul>

    </div>

  </div>
</div>

<? include 'shared/blue-box.php' ?>