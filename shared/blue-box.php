<div class="blue-box">
  <div class="container">
    <div class="grid">

      <div class="grid-cell t50">
        <a href="index.php?url=home" class="logo">
          <img src="assets/images/logo-namidia-white.png" alt="Namídia" />
        </a>
        <? include 'shared/menu.php' ?>
      </div>

      <div class="grid-cell t50">
        <ul class="list-social">
          <li><a href="#"><i class="icon i25 icon-2"></i></a></li>
          <li><a href="#"><i class="icon i25 icon-3"></i></a></li>
          <li><a href="#"><i class="icon i25 icon-5"></i></a></li>
          <li><a href="#"><i class="icon i25 icon-4"></i></a></li>
          <li><a href="#"><i class="icon i25 icon-6"></i></a></li>
          <li><a href="#"><i class="icon i25 icon-7"></i></a></li>
        </ul>
      </div>

    </div>
  </div>
</div>