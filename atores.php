<? include 'shared/banner.php' ?>

<div class="box box-1">
  <div class="container">

    <div class="title-box">
      <h2><span>Artistas</span></h2>
      <p>Nós contamos com o melhor banco de atores do brasil, dos iniciantes aos mais profissionais. <br/>Verdadeiros astros da mídia nacional e internacional.</p>
    </div>

    <nav class="internal-navigation">
      <ul class="tabs-action">
        <li><a href="index.php?url=" class="active">talentos</a></li>
        <li><a href="index.php?url=">atores</a></li>
        <li><a href="index.php?url=">atrizes</a></li>
        <li><a href="index.php?url=">modelos</a></li>
        <li><a href="index.php?url=">artistas</a></li>
        <li><a href="index.php?url=">músicos</a></li>
        <li><a href="index.php?url=">figuração</a></li>
      </ul>
    </nav>

    <form class="search-bar" method="get" action="index.php?url=">

      <div class="row name">
        <input class="input-text small" id="name" type="text" placeholder="Nome">
      </div>

      <div class="row">
        <select class="input-text" id="idade" name="idade">
          <option value="" selected>Idade</option>
          <option value="0_a_10">De 0 a 10 anos</option>
          <option value="10_a_20">De 10 a 20 anos</option>
          <option value="20_a_30">De 20 a 30 anos</option>
        </select>
      </div>

      <div class="row">
        <select class="input-text" id="cabelo" name="cabelo">
          <option value="" selected>Cabelo</option>
          <option value="castanho">Castanho</option>
          <option value="loiro">Loiro</option>
        </select>
      </div>

      <div class="row">
        <select class="input-text" id="olhos" name="olhos">
          <option value="" selected>Olhos</option>
          <option value="Azuis">Azuis</option>
          <option value="Verdes">Verdes</option>
        </select>
      </div>

      <div class="row">
        <select class="input-text" id="cor-da-pele" name="cor-da-pele">
          <option value="" selected>Cor da pele</option>
          <option value="Clara">Clara</option>
          <option value="Escura">Escura</option>
        </select>
      </div>

      <div class="button-bar">
        <button class="button button-gray" type="submit">Buscar</button>
      </div>

    </form>

    <ul class="list-talent">
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-1.jpg" alt="Talento 1" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-2.jpg" alt="Talento 2" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-3.jpg" alt="Talento 3" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-4.jpg" alt="Talento 4" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-5.jpg" alt="Talento 5" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-1.jpg" alt="Talento 1" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-1.jpg" alt="Talento 1" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-1.jpg" alt="Talento 1" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-2.jpg" alt="Talento 2" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>

      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-1.jpg" alt="Talento 1" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          <img src="assets/images/talentos/im-talento-2.jpg" alt="Talento 2" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-3.jpg" alt="Talento 3" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-4.jpg" alt="Talento 4" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-5.jpg" alt="Talento 5" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-1.jpg" alt="Talento 1" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
      <li>
        <a href="index.php?url=perfil-description">
          <img src="assets/images/talentos/im-talento-2.jpg" alt="Talento 2" />
          <div class="group-text">
            <span class="name">Roberto Marques</span>
            <span class="age">22 anos</span>
            <hr/>
            <span class="skills">Ator - Modelo - Figurante</span>
          </div>
        </a>
      </li>
    </ul>

    <div class="pagination">
      <ul>
        <li><a href="#"><i class="icon i20 icon-3"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-3"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-3"></i></a></li>
        <li class="actived"><a href="#"><i class="icon i20 icon-4"></i></a></li>
        <li><a href="#"><i class="icon i20 icon-3"></i></a></li>
      </ul>
    </div>

  </div>
</div>

<? include 'shared/blue-box.php' ?>