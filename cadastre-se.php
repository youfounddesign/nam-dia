<div class="ajax-page sign-up">
  <div class="page-content">

    <a href="javascript:void(0);" class="close-ajax">
      <i class="icon i25 icon-17"></i>
    </a>

    <div class="title-box">
      <h2>Cadastre-se</h2>
      <p>Bem vindo! Preencha o formulário e junte-se a esta grande<br/> equipe de sucesso.</p>
    </div>

    <form method="post" action="">

      <div class="row">
        <input type="text" class="input-text" placeholder="Nome *" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Sobrenome *" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Telefone" phone />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Celular" phone />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Data de nascimento *" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Cidade *" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Email *" />
      </div>

      <div class="row">
        <input type="text" class="input-text" placeholder="Confirmar email *" />
      </div>

      <div class="row">
        <input type="password" class="input-text" placeholder="Senha *" />
      </div>

      <div class="row">
        <input type="password" class="input-text" placeholder="Confirmar senha *" />
      </div>

      <div class="button-bar">
        <input type="submit" value="Enviar" class="button large center" />
      </div>

    </form>

  </div>
</div>