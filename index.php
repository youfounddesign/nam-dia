<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="pt-BR" dir="ltr"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="pt-BR" dir="ltr"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="pt-BR" dir="ltr"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="pt-BR" dir="ltr"> <!--<![endif]-->
	<head>
		
		<!-- CSS -->
		<link rel="stylesheet" href="assets/stylesheets/sass/all.css.php" type="text/css"><!-- layout -->

		<title>Namídia | Casting de atores - Os melhores atores e atrizes</title>
		<meta charset="utf-8" />

		<meta name="keywords" content="namidia, casting, atores, atrizes">
		<meta property="og:title" content="Namídia"/>
		<meta name="description" content="Casting de atores - Os melhores atores e atrizes">

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

		<!-- Favicons -->
		<link rel="shortcut icon" href="favicon.ico">
		<meta property="og:image" content="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/images/favicon-100x100.png"/><!-- Imagem face -->

		<!-- Jquery -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="assets/javascripts/jquery.js"><\/script>')</script>

		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Fonte -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:900,400,700,500,300,100' rel='stylesheet' type='text/css'>

	</head>
	<?php $url = $_GET["url"]; ?>
	<body class="site <?php echo $url ?><?php if($url == '') {echo 'home';} ?>">


		<!-- Header -->
		<? include 'shared/header.php' ?>

		<!-- Menu -->
		<nav class="fixed-navigation">
			<? include 'shared/menu.php' ?>
		</nav>

		<!-- Ajax pages -->
		<div class="hide-pages"></div>

		<!-- Conteúdo -->
		<section class="content">
			<?
				$page = $_GET['url'];
					if(!empty($page)){
					$ext = ".php";
					if(file_exists($page . $ext)) {
					include($page . $ext); 
					} else { include '404.php'; }
				} else { include ("home.php"); }
			?>
		</section>

		<!-- Footer -->
		<? include 'shared/footer.php' ?>

		<!-- Javascript -->
		<script src="assets/javascripts/jquery.bxslider.min.js"></script><!-- Bxslider -->
		<script src="assets/javascripts/masked-form.js"></script><!-- Mask form -->
		<script src="assets/javascripts/popup.js"></script><!-- Popup -->
		<script src="assets/javascripts/all.js"></script><!-- Scripts gerais -->
		
		<!-- Google Analytics -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>

	</body>
</html>