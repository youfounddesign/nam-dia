<? include 'shared/banner.php' ?>

<div class="container">
  <div class="page-content">

    <div class="grid top-bar">
      <div class="grid-cell t65">
        <h3 class="subtitle">Roberto<br> Marques</h3>
      </div>
      <div class="grid-cell t35">
        <nav class="internal-navigation">
          <ul class="tabs-action">
            <li><a href="index.php?url=perfil-description">Fotos e descrição</a></li>
            <li><a href="index.php?url=perfil-videos">Vídeos</a></li>
            <!-- <li><a href="index.php?url=perfil-imprensa">Imprensa</a></li> -->
            <li><a href="index.php?url=perfil-curriculo">Currículo</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="content-bar">

      <div class="content-bar">

          <!-- if the information is empty, show this div
            <div class="empty">
              <p>Clique em editar para preencher os dados do seu perfil.</p>
            </div>
          end -->

          <div class="grid">
            <div class="grid-cell t40">
              <img src="admin/assets/images/user-photos/user-big-1.jpg" height="307" width="383" alt="Roberto Marques" />
            </div>
            <div class="grid-cell t60">

              <div class="curriculo-content">
                <p>
                  NOVELAS
                  <br/><br/>
                  2010 – TITITI  (remake) – Rede Globo<br/>
                  Autor: Cassiano Gabus Mendes – adaptação: Maria Adelaide Amaral<br/>
                  Direção: Jorge Fernando<br/>
                  Personagem: Armandinho<br/>
                  <br/><br/>
                  2009 – CARAS E BOCAS – Rede Globo<br/>
                  Autor: Walcyr Carrasco<br/>
                  Direção: Jorge Fernando<br/>
                  Personagem: Tadeu<br/>
                  <br/><br/>
                  2007/2008 – DUAS CARAS – Rede Globo<br/>
                  Autor: Aguinaldo Silva<br/>
                  Direção: Wolf Maya<br/>
                  Personagem: Heraldo<br/>
                  <br/><br/>
                  2003/2007 – MALHAÇÃO – Rede Globo<br/>
                  Autor: diversos<br/>
                  Direção: Ricardo Waddington – Direção de Núcleo<br/>
                  Personagem: Kiko<br/>
                  <br/><br/>
                  2002 – O BEIJO DO VAMPIRO – Rede Globo<br/>
                  Autor: Antônio Calmon<br/>
                  Direção: Marcos Paulo<br/>
                </p>
              </div>

            </div>
          </div>

      </div>
    </div>

  </div>
</div>

<? include 'shared/blue-box.php' ?>