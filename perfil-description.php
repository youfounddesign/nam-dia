<? include 'shared/banner.php' ?>

<div class="container">
  <div class="page-content">

    <div class="grid top-bar">
      <div class="grid-cell t65">
        <h3 class="subtitle">Roberto<br> Marques</h3>
      </div>
      <div class="grid-cell t35">
        <nav class="internal-navigation">
          <ul class="tabs-action">
            <li><a href="index.php?url=perfil-description" class="active">Fotos e descrição</a></li>
            <li><a href="index.php?url=perfil-videos">Vídeos</a></li>
            <!-- <li><a href="index.php?url=perfil-imprensa">Imprensa</a></li> -->
            <li><a href="index.php?url=perfil-curriculo">Currículo</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="content-bar">

      <!-- if the information is empty, show this div
        <div class="empty">
          <p>Clique em editar para preencher os dados do seu perfil.</p>
        </div>
      end -->

      <div class="grid">
        <div class="grid-cell t65">

          <div class="slider-photos">

            <ul data-slider-photos>
              <li><img src="admin/assets/images/user-photos/user-big-1.jpg" height="492" width="614" alt="Roberto Marques" /></li>
              <li><img src="admin/assets/images/user-photos/user-big-2.jpg" height="492" width="614" alt="Roberto Marques" /></li>
              <li><img src="admin/assets/images/user-photos/user-big-1.jpg" height="492" width="614" alt="Roberto Marques" /></li>
              <li><img src="admin/assets/images/user-photos/user-big-2.jpg" height="492" width="614" alt="Roberto Marques" /></li>
              <li><img src="admin/assets/images/user-photos/user-big-1.jpg" height="492" width="614" alt="Roberto Marques" /></li>
              <li><img src="admin/assets/images/user-photos/user-big-2.jpg" height="492" width="614" alt="Roberto Marques" /></li>
              <li><img src="admin/assets/images/user-photos/user-big-1.jpg" height="492" width="614" alt="Roberto Marques" /></li>
              <li><img src="admin/assets/images/user-photos/user-big-2.jpg" height="492" width="614" alt="Roberto Marques" /></li>
            </ul>

            <div class="slider-photos-controls">
              <a data-slide-index="0" href=""><img src="admin/assets/images/user-photos/user-small-1.jpg" height="114" width="143" alt="Roberto Marques" /></a>
              <a data-slide-index="1" href=""><img src="admin/assets/images/user-photos/user-small-2.jpg" height="114" width="143" alt="Roberto Marques" /></a>
              <a data-slide-index="2" href=""><img src="admin/assets/images/user-photos/user-small-1.jpg" height="114" width="143" alt="Roberto Marques" /></a>
              <a data-slide-index="3" href=""><img src="admin/assets/images/user-photos/user-small-2.jpg" height="114" width="143" alt="Roberto Marques" /></a>
              <a data-slide-index="4" href=""><img src="admin/assets/images/user-photos/user-small-1.jpg" height="114" width="143" alt="Roberto Marques" /></a>
              <a data-slide-index="5" href=""><img src="admin/assets/images/user-photos/user-small-2.jpg" height="114" width="143" alt="Roberto Marques" /></a>
              <a data-slide-index="6" href=""><img src="admin/assets/images/user-photos/user-small-1.jpg" height="114" width="143" alt="Roberto Marques" /></a>
              <a data-slide-index="7" href=""><img src="admin/assets/images/user-photos/user-small-2.jpg" height="114" width="143" alt="Roberto Marques" /></a>
            </div>

          </div>

        </div>
        <div class="grid-cell t35">

          <div class="info">
            <p>Olá, sou o Roberto. Trabalho como ator a mais de 5 anos, com passagem em grandes novelas e  peças de teatro. Meu objetivo principal é ser  reconhecido internacionalmente e poder atuar em grandes filmes do cinema.
            <ul class="list-infos">
              <li><strong>Nome Completo: </strong>Roberto Souza Marques</li>
              <li><strong>Matrícula: </strong>00214</li>
              <li><strong>Idade: </strong>26 anos</li>
              <li><strong>Cidade: </strong>Rio de Janeiro</li>
              <li><strong>Cabelos: </strong>Pretos - Cacheados</li>
              <li><strong>Olhos: </strong>Castanhos - Claros</li>
              <li><strong>Altura: </strong>1,80 m</li>
              <li><strong>Peso: </strong>75 Kg</li>
              <li><strong>Calçados: </strong>42</li>
              <li><strong>Manequim: </strong>40</li>
              <li><strong>Website: </strong>www.robertomarques.com.br</li>
            </ul>
            <ul class="list-social">
              <li><a href="#"><i class="icon i25 icon-2"></i></a></li>
              <li><a href="#"><i class="icon i25 icon-3"></i></a></li>
              <li><a href="#"><i class="icon i25 icon-5"></i></a></li>
              <li><a href="#"><i class="icon i25 icon-4"></i></a></li>
              <li><a href="#"><i class="icon i25 icon-6"></i></a></li>
              <li><a href="#"><i class="icon i25 icon-7"></i></a></li>
            </ul>
          </div>

          <div class="button-bar">
            <a href="index.php?url=edit-perfil-geral" class="button button-gray large">Contratar</a>
          </div>

        </div>
      </div>

    </div>

  </div>
</div>

<? include 'shared/blue-box.php' ?>